module.exports = {
  purge: {    
    content: [
      "../../../../radix/toolbox/csq-logbook-web/ClientApp/src/app/app-web/**/*.html",
      "../../../../radix/toolbox/csq-logbook-web/ClientApp/src/app/app-web/**/*.ts",
    ],
  },
  important: true,
  prefix: "tw-",
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      colors: {
        "csq-grey": {
          DEFAULT: "#EEEEEE",
          50: "#FFFFFF",
          100: "#FFFFFF",
          200: "#FFFFFF",
          300: "#FFFFFF",
          400: "#FFFFFF",
          500: "#EEEEEE",
          600: "#D4D4D4",
          700: "#BBBBBB",
          800: "#A2A2A2",
          900: "#888888",
        },
      },
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
};
